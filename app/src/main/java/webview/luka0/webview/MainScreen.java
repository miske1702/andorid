package webview.luka0.webview;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.webkit.WebChromeClient;
import android.widget.Toast;
import android.widget.ProgressBar;


public class MainScreen extends AppCompatActivity {
     private WebView mWebView;
    private Button mDugme;
    private EditText edit;
    private ProgressBar progress;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getWindow().requestFeature(Window.FEATURE_PROGRESS);
        setContentView(R.layout.activity_main_screen);

        getWindow().setFeatureInt( Window.FEATURE_PROGRESS, Window.PROGRESS_VISIBILITY_ON);

        mWebView = (WebView) findViewById(R.id.activity_main_webview);
        WebSettings webSettings= mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        progress = (ProgressBar) findViewById(R.id.progressBar);
        progress.setMax(100);

        mDugme = (Button) findViewById(R.id.dugme);

        edit=(EditText) findViewById(R.id.editText);

        mDugme.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View view)
                    {


                        String s = edit.getText().toString();
                       /*) if (!s.contains("http")){
                            String novis="https://";
                        public static String insert (String s,String novis, int index){
                                String pocetak=s.substring(0,index);
                                String kraj =s.substring(index);
                                return pocetak+novis+kraj;

                            }
                    }*/
                        // blizu si bio :)
                        if (s.toLowerCase().contains("http") || s.toLowerCase().contains("https"))
                        {

                            mWebView.loadUrl(s);
                            MainScreen.this.progress.setProgress(0);
                        }
                        else
                        {
                            String s1 = "http://" + s;
                            mWebView.loadUrl(s1);
                            MainScreen.this.progress.setProgress(0);
                        }


                    }
                }
        );

        final Activity MyActivity = this;
        mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress)
            {
                //Make the bar disappear after URL is loaded, and changes string to Loading...
                MyActivity.setTitle("Loading...");
                MyActivity.setProgress(progress * 100); //Make the bar disappear after URL is loaded

                // Return the app name after finish loading
                if(progress == 100)
                    MyActivity.setTitle(R.string.app_name);
            }
        });
//        mWebView.setWebViewClient(new WebViewClient());

        mWebView.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(MyActivity, "Oh no! " + description, Toast.LENGTH_SHORT).show();
            }
        });


    }
}
